var electronInstaller = require('electron-winstaller');
resultPromise = electronInstaller.createWindowsInstaller({
    appDirectory: './build/library_sibsau-win32-ia32',
    outputDirectory: './build/windows-installer',
    authors: 'My App Inc.',
    exe: 'library_sibsau.exe',
    setupIcon: 'C:/Users/Andrey/Documents/gui-electron-library-subsau/build/icons/icon.ico',
    iconUrl: 'C:/Users/Andrey/Documents/gui-electron-library-subsau/build/icons/icon.ico',
    name: 'Library-Sibsau'
  });

resultPromise.then(() => console.log("It worked!"), (e) => console.log(`No dice: ${e.message}`));