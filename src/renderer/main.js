import Vue from 'vue'
import axios from 'axios'
import App from './App'
import router from './router'
import store from './store'
import Library from '../../static/Library'

if (!process.env.IS_WEB) Vue.use(require('vue-electron'))
Vue.http = Vue.prototype.$http = axios
Vue.config.productionTip = false
process.env['ELECTRON_DISABLE_SECURITY_WARNINGS'] = true
/* eslint-disable */
window.jQuery = window.$ = require('jquery/dist/jquery.min')
import PopperJs from 'popper.js/dist/umd/popper.js';
import 'bootstrap/dist/js/bootstrap.min'
import 'bootstrap/dist/css/bootstrap.min.css'
/* eslint-enable */
/* eslint-disable no-new */
Vue.prototype.$library = new Library()
Vue.prototype.$books = Vue.prototype.$library.books
new Vue({
  components: { App },
  router,
  store,
  template: '<App/>'
}).$mount('#app')
