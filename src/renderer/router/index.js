import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'index',
      component: require('@/components/index/index.vue').default
    },
    {
      path: '/add_book',
      name: 'add_book',
      component: require('@/components/add_book.vue').default
    },
    {
      path: '/book/:id',
      name: 'book_id',
      component: require('@/components/book.vue').default
    },
    {
      path: '/:author',
      name: 'book_author',
      component: require('@/components/index/author.vue').default
    },
    {
      path: '/:tag',
      name: 'book_tag',
      component: require('@/components/index/tag.vue').default
    },
    {
      path: '/inventory',
      name: 'inventory',
      component: require('@/components/inventory.vue').default
    },
    {
      path: '*',
      redirect: '/'
    }
  ]
})
