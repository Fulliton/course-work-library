import db from './../src/renderer/datastore.js'
import Book from './Book.js'
export default class Library {
  constructor () {
    this.books = []
    db.find({}, (err, newDoc) => {
      if (err) {
        return console.log(err)
      }
      newDoc.forEach((d) => {
        this.books.push(
          new Book(d.id, d.name, d.author, d.description, d.publisher, d.year, d.tags, d.stars, d.language, d.count, d.repair, d.lost, d.comments)
        )
      })
    })
  }
  addBook (object) {
    this.books.push(object)
    db.insert(object, function (err, docs) {
      if (err) {
        return console.log(err)
      }
      console.log(docs.id)
    })
  }
  removeBook (id) {
    this.books.forEach((book) => {
      if (book.id === Number(id)) {
        db.remove(book, (err, e) => {
          if (err) {
            return console.log(err)
          } else {
            console.log(e)
            this.books.splice(this.books.indexOf(book), 1)
          }
        })
      }
    })
  }
  get inventory () {
    let countBooks = 0
    let lostBooks = 0
    let repairBooks = 0
    let deltaStarsBooks = 0
    let countAuthor = []
    let countTags = []
    let countComments = 0
    this.books.forEach((book) => {
      countBooks += Number(book.count)
      lostBooks += Number(book.lost)
      repairBooks += Number(book.repair)
      deltaStarsBooks += Number(book.stars)
      countComments += book.comments.length
    })
    deltaStarsBooks = deltaStarsBooks / this.books.length
    deltaStarsBooks = deltaStarsBooks.toFixed(1)
    let i = 0
    while (i < this.books.length) {
      if (!countAuthor.includes(this.books[i].author)) {
        countAuthor.push(this.books[i].author)
      }
      i++
    }
    i = 0
    while (i < this.books.length) {
      this.books[i].tags.forEach((item) => {
        if (!countTags.includes(item)) {
          countTags.push(item)
        }
      })
      i++
    }
    return {
      countBooks: countBooks,
      lostBooks: lostBooks,
      repairBooks: repairBooks,
      deltaStarsBooks: deltaStarsBooks,
      countAuthor: countAuthor,
      countTags: countTags,
      countComments: countComments
    }
  }
}
