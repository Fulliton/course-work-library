import db from './../src/renderer/datastore.js'
export default class Book {
  constructor (id, name, author, description, publisher, year, tags = null, stars, language, count, repair = null, lost = null, comments) {
    this.id = id
    this.name = name
    this.author = author
    this.description = description
    this.publisher = publisher
    this.year = year
    this.tags = tags
    this.stars = stars
    this.language = language
    this.count = count
    this.comments = comments
    this.repair = repair
    this.lost = lost
  }
  update (type, value) {
    console.log(this[type])
    this[type] = value
    db.update({id: this.id}, {$set: {[type]: this[type]}}, {}, (err) => {
      if (err) {
        return console.log(err)
      }
      console.log('update')
    })
    db.find({}, (err, newDoc) => {
      if (err) {
        return console.log(err)
      }
      newDoc.forEach((d) => {
        if (d.id === this.id) {
          console.log(d[type])
        }
      })
    })
  }
  addComment (comment) {
    this.comments.push(comment)
    db.update({id: this.id}, {$push: {comments: comment}}, {}, (err) => {
      if (err) {
        return console.log(err)
      }
      console.log('update')
    })
  }
}
